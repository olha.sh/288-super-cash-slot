import {Dimensions} from 'react-native'

export default Constants = {
    MAX_WIDTH: Dimensions.get('screen').width,
    MAX_HEAGHT: Dimensions.get('screen').height,
    XR: Dimensions.get('screen').width / 1439,
    Reels: 3,
    Reel_Repeat: 10,
    Symbols: 3,
    Reel_Symbols: [
        'ALSJAGDKAHDHLDFJSLAD',
        'HFJDKALSKDFJHJSKADFA',
        'DKLJKLASDJFKDJLSJALD'
    ],
    BETS: [10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000],
    LINES: [
        [
            [0, 0], [1, 0], [2,0]
        ],
        [
            [0, 1], [1, 1], [2,1]
        ],
        [
            [0, 2], [1, 2], [2,2]
        ],
        [
            [0, 0], [1, 1], [2,2]
        ],
        [
            [0, 2], [1, 1], [2,0]
        ],
        [
            [0, 0], [1, 0], [2,0]
        ],
        [
            [0, 0], [1, 2], [2,0]
        ],
        [
            [0, 2], [1, 0], [2,2]
        ],
        [
            [0, 1], [1, 0], [2,1]
        ],
    ]
}

