import React, { useEffect, useState } from 'react';
import {
    View,
  } from 'react-native';
import {PlayScreen} from './screens/PlayScreen/PlayScreen'
import {StartScreen} from './screens/StartScreen/StartScreen'
import { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";

function Game(){
const [isPlay, setIsPlay] = useState(false)
 console.log(" GAME  GAME  GAME GAME GAME  ")
    return(
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <OrientationLocker orientation={LANDSCAPE}
        onChange={orientation => console.log('onChange', orientation)}
        onDeviceChange={orientation => console.log('onDeviceChange', orientation)}/>
                {isPlay? (
                <PlayScreen/> 
                ):(
                <StartScreen startGame={()=>setIsPlay(true)}/>
                )}
        
            
        </View>
    )
}

export{
    Game
}