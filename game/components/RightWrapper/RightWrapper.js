import React from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  Image,
  Text
} from 'react-native';
import {TouchButton} from '../../components/Touch/Touch'
import Constants from '../../Constants ';

function RightWrapper({betValue, handleSpin, handleMaxBet, handleMinBet}){
    return(
        <View style={styles.rightContainer}>
          <ImageBackground 
          source={require('../../assets/img/betContainer.png')}
          style={styles.betContainer}
          resizeMode='contain'
          >
            <TouchButton
              styleTouch={styles.betBtn}
              handleTouch={handleMinBet}
              url={require('../../assets/img/minBetBtn.png')}
              styleImg={styles.betBtnImg}
            />
          <View style={styles.betContent}>
            <Image source={require('../../assets/img/betIcon.png')} style ={styles.betIcon}/>
            <Text style={styles.betText}>{betValue}</Text>
          </View>
          <TouchButton
                styleTouch={styles.betBtn}
                handleTouch={handleMaxBet}
                url={require('../../assets/img/maxBetBtn.png')}
                styleImg={styles.betBtnImg}
            />

        </ImageBackground>

        <TouchButton 
          styleTouch={styles.touch}
          handleTouch={handleSpin}
          url={require('../../assets/img/spin-btn.png')}
          styleImg={styles.spinBtn}
        />
        </View>
        
    )
}

const styles = StyleSheet.create({
    rightContainer: {
        // marginLeft: Constants.XR*15,
        justifyContent: 'center',
      },
      betContainer: {
        width: Constants.XR*250,
        height: Constants.XR*100,
    
        flexDirection: 'row',
        justifyContent :'center',
        alignItems: 'center',
    },

    betBtn: {
        width: Constants.XR*30,
        height: Constants.XR*40
      },
    betBtnImg: {
        width: '100%',
        height: '100%'
    },
    betText: {
        color: '#fff',
        marginLeft: Constants.XR*15,
        fontSize: 18,
        fontWeight: 'bold',
    },
    betIcon:{
        width: Constants.XR*40,
        height: Constants.XR*40,
    },
    betContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: Constants.XR*15,
        marginRight: Constants.XR*15
    },
    touch: {
        marginTop: Constants.XR*30,
        alignItems: 'center',
      },
    spinBtn: {
        width: Constants.XR*130,
        height: Constants.XR*130
      }

    
})

export{
    RightWrapper
}