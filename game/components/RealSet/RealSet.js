import React, { Component } from "react";
import {
  ImageBackground,
    StyleSheet,
    View,
  } from 'react-native';
import {Reel} from '../Real/Real'
import Constants from '../../Constants '

  class ReelSet extends Component{
      constructor(props){
          super(props);
          this.state = {
              width: null,
              height: null
          };
          this.reels = [];
          this.reelsInMotion = null;
          this.spinResults = [];
          this.winLines = [];
      }
      randomBetween = (min, max) => {
          return Math.floor(Math.random()*(max - min + 1) + min)
      }

      heightLightWinningLines = (currentInx) => {

        if(!this.winLines.length){
          return
        }

        if(currentInx > 0){
          Constants.LINES[this.winLines[currentInx - 1]].map((el) => {
            this.reels[el[0]].hightLightIndex(el[1], false);
            this.reels[el[0]].shakeAtIndex(el[1])
          })
        }

        if(currentInx > this.winLines.length - 1){
          return
        }

        Constants.LINES[this.winLines[currentInx]].map((el) => {
          console.log("win")
          this.reels[el[0]].hightLightIndex(el[1], true);
          this.props.handleScore(this.winLines.length)
        })

        setTimeout(() => {
          this.heightLightWinningLines(currentInx + 1)
        }, 800)

      }

      evaluateResults = () => {
        this.winLines = [];
        

        for(let lineIdx = 0; lineIdx < Constants.LINES.length; lineIdx++){
          let streak = 0;
          let currentKind = null;

          for( let coordIdx = 0; coordIdx < Constants.LINES[lineIdx].length; coordIdx++){
            let coords = Constants.LINES[lineIdx][coordIdx];
            let symbolAtCoords = this.spinResults[coords[0]][coords[1]]

            if (coordIdx === 0){
              if (symbolAtCoords === "D"){
                break
              }

              currentKind = symbolAtCoords;
              streak = 1;
            } else{
              if(symbolAtCoords !==currentKind){
                break
              }
              streak +=1;
            }
          }

          if (streak >= 3){
            this.winLines.push(lineIdx)
          }
        }

        console.log(this.winLines)
        this.heightLightWinningLines(0)
      }

      spin = () => {
        this.reelsInMotion = Constants.Reels;
          for(let i=0; i< Constants.Reels; i++ ){
            

              let offsetValue = this.randomBetween(
                (Constants.Reel_Repeat - 6) * this.reels[i].symbol.length,
                (Constants.Reel_Repeat - 5) * this.reels[i].symbol.length,
            )
            console.log('offsetValue '+ offsetValue)
              this.reels[i].scrollByOffset(offsetValue, (reelIdx, results)=>{
                this.reelsInMotion -= 1;
                this.spinResults[reelIdx] = results;

                if (this.reelsInMotion === 0){
                 this.evaluateResults()
                }
              }) 
          }
       
      }
      onLayout = (e) => {
        this.setState({
            width: e.nativeEvent.layout.width,
            height: e.nativeEvent.layout.height
        })
      }

      renderReels = (e) => {
          let ReelWidth = (this.state.width / Constants.Reels) - Constants.XR*50
          let ReelList = Array.apply(null, Array(Constants.Reels)).map((el, idx)=>{
            return(
                <Reel 
                  width={ReelWidth} 
                  height={Constants.XR*525} 
                  key={idx} 
                  index={idx} 
                  ref = {(ref)=>{this.reels[idx] = ref}}
                />
            )
          })
    
          return (
              <>
                {ReelList}
              </>
          )
          
      }

      render(){
          return(  
            <ImageBackground
              source={require('../../assets/img/slot-container.png')} 
              resizeMode='contain'
              style={{
                width: '100%', 
                height: Constants.XR*660,
                marginTop: Constants.XR*60,
                alignItems: 'center',
                justifyContent: 'center'
                }}
              >
            <View style = {[
              styles.reelSet, 
              { marginTop: Constants.XR*60, 
                width: Constants.XR*610,
                flexDirection: 'row',
                justifyContent: 'space-evenly'
                
              }]} 
              onLayout = {this.onLayout}
              >
               {this.state.width && this.state.height && this.renderReels()}
            </View>
            </ImageBackground>
                  
              
          )
      }
  }


  const styles = StyleSheet.create({
 
    reelSet: {
        flex: 1,
        flexDirection:'row',
        
    }
  })

  export{
      ReelSet
  }