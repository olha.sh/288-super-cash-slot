import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Animated
  } from 'react-native';
import Constants from '../../Constants '
import {SlotItem} from '../../components/SlotItem/SlotItem'



class Reel extends Component{
    constructor(props){
        super(props);
        this.symbol = Constants.Reel_Symbols[this.props.index];
        this.symbolHeight = this.props.height/ Constants.Symbols;
        this.symbolRepeat = this.symbol.repeat(Constants.Reel_Repeat).split("");
        this.position = this.symbolRepeat.length - Constants.Symbols;
        this.currentScrollPos = this.position * this.symbolHeight * -1;

        this.symbolRefs = [];

        this.state = {
            scrollPos: new Animated.Value(0)
        }
    }
    hightLightIndex = (index, hightLight) =>{
        this.symbolRefs[this.position + index].setActive(hightLight)
    }

    shakeAtIndex = (index) => {
        this.symbolRefs[this.position + index].shake()
    }

    scrollByOffset = (offset, callback) =>{
        for(let i = 0; i<Constants.Symbols; i++){
            this.symbolRefs[this.position + i].setActive(true)
        }
        this.currentScrollPos = this.currentScrollPos + (this.symbolHeight * offset)
        this.position = this.position - offset;
        Animated.timing(this.state.scrollPos, 
            {
                toValue: this.currentScrollPos,
                duration: 1500 + (this.props.index*250),
                useNativeDriver: true
            }).start(()=>{
                let result = [];
                this.position = ((Constants.Reel_Repeat - 2) * this.symbol.length) + (this.position % this.symbol.length)
                for(let i = 0; i<Constants.Symbols; i++){
                    this.symbolRefs[this.position + i].setActive(false)
                    result.push(this.symbolRepeat[this.position + i])
                }
                this.currentScrollPos = this.position * this.symbolHeight * -1;
                this.state.scrollPos.setValue(this.currentScrollPos)

                callback(this.props.index, result)
            })
    }

render(){
    return(
        <View style={[styles.reel, {width: this.props.width, height: this.props.height}]}>
            <Animated.View style={{width: this.props.width, height: this.symbolRepeat.length * this.symbolHeight, transform: [{translateY: this.state.scrollPos}]}}>
              {this.symbolRepeat.map((el, idx)=>{
                return(
                    <SlotItem 
                        symbol={el} 
                        width={this.props.width} 
                        height={this.symbolHeight} 
                        key={idx} 
                        id={idx} 
                        ref={(ref)=>{this.symbolRefs[idx]=ref}} 
                    />
                )
            })}  
            </Animated.View>
            
           
        </View>
    )
}

}



const styles = StyleSheet.create({
    reel: {
        overflow: 'hidden'
    }
})

export {
    Reel
}