import React  from "react";
import {
    Image,
    TouchableOpacity
  } from 'react-native';

function TouchButton({styleTouch, handleTouch, url, styleImg }){
    return(
        <TouchableOpacity style={styleTouch} onPress= {handleTouch}>
            <Image 
            source={url}
            resizeMode='contain'
            style={styleImg}
            />
        </TouchableOpacity>
    )
}


export {
    TouchButton
}
