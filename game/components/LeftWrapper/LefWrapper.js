import React from 'react';
import {
  StyleSheet,
  ImageBackground,
  Text,
  View, 
  Image
} from 'react-native';
import Constants from '../../Constants '

function LeftWrapper({scoreValue}){
    return(
         <View style={styles.leftWrapper}>
              <ImageBackground 
                  source={require('../../assets/img/scoreContainer.png')}
                  resizeMode='contain'
                  style={styles.scoreBlock}
              >
                  <Image source={require('../../assets/img/betIcon.png')} style ={styles.betIcon}/>
                  <Text style={styles.scoreText}>{scoreValue}</Text>
              </ImageBackground>
          </View>
        
    )
}

const styles = StyleSheet.create({
    scoreBlock: {
        width: Constants.XR*250,
        height: Constants.XR*100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    betIcon:{
        width: Constants.XR*45,
        height: Constants.XR*45,
    },
    scoreText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: Constants.XR*20
 
    }
})

export{
    LeftWrapper
}