import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Animated
  } from 'react-native';
import Constants from '../../Constants '



class SlotItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            active: true,
            animatedValue: new Animated.Value(0)

        }
    }

    getImage = () => {
        switch(this.props.symbol){
            case 'A': 
                return require("../../assets/img/slot-item1.png");
            case 'S': 
                return require("../../assets/img/slot-item2.png");
            case 'D': 
                return require("../../assets/img/slot-item3.png");
            case 'F': 
                return require("../../assets/img/slot-item4.png");
            case 'G': 
                return require("../../assets/img/slot-item5.png");
            case 'H': 
                return require("../../assets/img/slot-item6.png");
            case 'J': 
                return require("../../assets/img/slot-item5.png");
            case 'K': 
                return require("../../assets/img/slot-item6.png");
            case 'L': 
                return require("../../assets/img/slot-item7.png");
    }
}

setActive = (active) =>{
    this.setState({
        active: active,
    })
}

shake = () => {
    this.state.animatedValue.setValue(0);
    Animated.timing(
        this.state.animatedValue,
        {
            toValue: 1,
            duration: 750,
            useNativeDriver: true
        }
    ).start();
}

render(){
    let SymbolSourse = this.getImage()
    let symbolAnimation = [
        {
            scale: this.state.animatedValue.interpolate({
                inputRange: [0, 0.25, 0.5, 1],
                outputRange: [1, 1.25, 0.75, 1]
            })
        },
        {
            rotate: this.state.animatedValue.interpolate({
                inputRange: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                outputRange: ['0deg', '15deg', '0deg', '-15deg', '0deg', '15deg', '0deg', '-15deg', '0deg', '15deg', '0deg']
            })
        }
    ]
    return(
        <View style={[styles.slotItem, {width: this.props.width, height: this.props.height}]}>
           <Animated.Image 
                style={{
                    width: this.props.width - 10, 
                    height: this.props.height - 10, 
                    opacity: this.state.active ? 1 : 0.6,
                    transform: symbolAnimation
                 }}  
                source={SymbolSourse}  
                resizeMode="contain"
            />
        </View>
    )
}

}



const styles = StyleSheet.create({
    slotItem: {
        // backgroundColor: "green"
    }

})

export {
    SlotItem
}