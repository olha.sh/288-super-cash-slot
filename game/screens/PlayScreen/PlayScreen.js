import React, { Component} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground
} from 'react-native';
import Constants from '../../Constants '
import {ReelSet} from '../../components/RealSet/RealSet' 
import {RightWrapper} from '../../components/RightWrapper/RightWrapper'
import {LeftWrapper} from '../../components/LeftWrapper/LefWrapper'

class PlayScreen extends Component{

  constructor(props){
    super(props);
    this.reelSet = null;
    this.state = {
      bet: 100,
      score: 1000
    }
    this.profitBet = 50
}
spin = () => {
  if(this.state.score - this.state.bet < 0){
    return
  }
  this.setState({
    score: this.state.score - this.state.bet
  }, ()=>{
    this.reelSet.spin();
  })

}
maxBet = () =>{
  let value = this.state.bet + this.profitBet
  this.setState({bet: value })
}

minBet = () =>{
  if(this.state.bet>= 50){
    let value = this.state.bet - this.profitBet
    this.setState({bet: value })
  }
  
}

winScore = (number) =>{
  let newScore = this.state.score + (this.state.bet * number)
  this.setState({
    score: newScore
  })
}

render(){
  return(
    <ImageBackground 
      source={require('../../assets/img/play-bg.png')}
      style={styles.playScreen}
    >

      <View style={styles.leftSide}>
        <LeftWrapper scoreValue={this.state.score}/>
      </View>

      <View style={styles.slotContainer}>
          <ReelSet 
            handleScore={this.winScore} 
            ref={(ref)=> {this.reelSet = ref}}
          />
      </View>

      <View style={styles.rightSide}>
          <RightWrapper 
          betValue={this.state.bet} 
          handleSpin={this.spin}
          handleMaxBet={this.maxBet}
          handleMinBet={this.minBet}
          />
      </View>

    </ImageBackground>
)
}

}

const styles = StyleSheet.create({
  playScreen: {
    flex: 1,
    flexDirection: 'row',
  },
  leftSide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -Constants.XR*150,
    
   },
  slotContainer: {
    flex: 2,
  },
  rightSide:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  
  });

export {
    PlayScreen
}