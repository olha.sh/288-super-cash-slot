import React from 'react';
import {
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity
} from 'react-native';

function StartScreen({startGame}){


  console.log("START SCRENN START SCRENN ")
    return(
      <ImageBackground style={styles.startScreen} source={require('../../assets/img/start-bg.png')}>
          <TouchableOpacity onPress={startGame}>
              <Image 
                style={styles.startBtn}
                source={require('../../assets/img/start-btn.png')}
            />
          </TouchableOpacity>
      </ImageBackground>
    )
}

const styles = StyleSheet.create({
    startScreen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export{
    StartScreen
}