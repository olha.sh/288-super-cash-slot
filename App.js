import React, {useState} from 'react';
import {
  StyleSheet,
} from 'react-native';
import {Game} from './game'
import { WebView } from 'react-native-webview';
import {Linking } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appsflyer from "react-native-appsflyer";
import messaging from '@react-native-firebase/messaging';
import OneSignal from 'react-native-onesignal';
import messageHandler from './messageHandler';
import {Loading} from './screen/loader'


appsflyer.initSdk(
  {
    devKey: 'AAwd5E9yMyjW2CdmPTgLJQ',
    isDebug: false, 
  });

 
//   const eventValues = {
//     af_content_id: "id123",
//   };
//   const eventName = ['registration', 'dep'];
//  eventName.forEach((el)=>{
//   appsflyer.logEvent(
//     el,
//     eventValues,
//     (res) => {
//       console.log(`${el} ` +res);
//     },
//     (err) => {
//       console.error(err);
//     }
//   );
//  })

  export default class App extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        url: undefined,
        source: undefined,
        isLoaded: false,
        loadLink: true,
        deepLink: undefined,
        uuidFirebase: null,
        uidappsflyer: null,
        isInvalid: false,
        pack: 'com.pastel.envy',
        link: 'https://f265u45bhh.execute-api.eu-central-1.amazonaws.com'
      }
     
    }
    
    getData = async (key) => {
      try {
        let value = await AsyncStorage.getItem(key)
        return JSON.parse(value)
      } catch(e) {
        console.log(e)
      }
    }
  
    setData = async(value, key) => {
      try {
        const newValue = await JSON.stringify(value)
        await AsyncStorage.setItem(key, newValue)
      } catch (e) {
        console.log(e)
      }
    }
    
    handleOrganicUser = async () => {
      console.log("handle organic user");
      await this.setData("isOrganic", "true");
      try {
        let response = await fetch(
          `${this.state.link}/real/organic`
        );
        let json = await response.json();
        let allowed = json.allowed;
        //test
        if(allowed) {
          console.log('allowed TRUE')
          this.setState({source:  `${this.state.link}/real?app=${this.state.pack}&uuid=${this.state.uuidFirebase}&uid=${this.state.uidappsflyer}`})
          this.setState({loadLink: false })
        } else {
          console.log("organic not allowed")
          this.setState({isInvalid: true})
          this.setData(true, 'isInvalid')
        }
      } catch (error) {
        console.error(error);
      }
    };
  
    

  requestNonOrganicUser = async (campaign) => {
      console.log("requestNonOrganicUser");	
        try {
          const params = campaign.split("_");
          console.log( 'param compain ' + params)
          if (params){
            console.log('We have params')
            this.setState({source: `${this.state.link}/real?app=${this.state.pack}&uuid=${this.state.uuidFirebase}&uid=${this.state.uidappsflyer}&a=${params[1]}&o=${params[2]}&s3=${params[3]}`})
            this.setState({loadLink: false })
            console.log("LINK " + this.state.source)
          
          } else{
            console.log('We dont have params')
            this.setState({source: `${this.state.link}/real?app=${this.state.pack}&uuid=${this.state.uuidFirebase}&uid=${this.state.uidappsflyer}`})
            this.setState({loadLink: false })
            console.log("LINK " + this.state.source)
            
          }
        } catch(err) {
          console.error(err);
          requestOrganicUser();
        }
    }
  
  async componentDidMount(){
  
    

    const invalid = await this.getData('isInvalid')
    this.setState({isInvalid: invalid})
    const initialUrl = await Linking.getInitialURL()
    console.log('initialUrl ' + initialUrl)
    if(initialUrl){
      this.setState({url: initialUrl})
      console.log('We have diplink ' + this.state.url)
    }
  
    appsflyer.getAppsFlyerUID(async (err, uid) => {
      if (err) {
       console.log(err)
      }
  
  this.setState({uidappsflyer: uid})
  const uuid = await messaging().getToken();
  this.setState({uuidFirebase: uuid})
  console.log("uuid is : " + uuid);
  
  });
  
  appsflyer.getAppsFlyerUID((err, appsflyerUID) => {
      if (err) {
        console.error(err);
      } else {
        console.log('on getappsflyerUID: ' + appsflyerUID);
        return appsflyerUID
      }
    });
  
    OneSignal.setAppId("f00d8a7c-4cdb-45ce-af0c-ee4679a37aa6");  
    let unsubscribe = messaging().onMessage(messageHandler);
    const registration = await this.getData('@isRegistered')
  
        console.log('registration ' + registration)
         const homeLink = await this.getData('@home')
         const trackLink = await this.getData('@track')
       
         if(registration){
           if(homeLink.includes('levelup')){
             
              this.setState({url: initialUrl })
              this.setState({isLoaded: true})
              await Linking.openURL(initialUrl)
              console.log(`GET DEEPLINK ${initialUrl}`)
           } else{
             this.setState({url: homeLink})
             this.setState({isLoaded: true})
           }
         }else{
          this.setState({url: trackLink})
          this.setState({isLoaded: true})
         }
   let onInstallConversionDataCanceller= appsflyer.onInstallConversionData(
      async ({data}) => {
        console.log('APSFLYER')
        console.log(data)
        if (JSON.parse(data.is_first_launch) == true){
          if (data.af_status === "Non-organic") {
            console.log("Non-organic")
                    await this.requestNonOrganicUser(data.campaign)
                  } else if (data.af_status === "Organic") {
                    console.log("Non-organic")
                    await this.handleOrganicUser();   
                }
  
                if(this.state.loadLink ===false){
                fetch(this.state.source)
                .then((response) => response.json())
                .then((data) =>{
                  this.setData(`${data.track}`, '@track')
                  this.setData(`${data.home}`, '@home')
                  this.setState({url: data.track})
                })
                .then(()=> this.setState({isLoaded: true}))
              } 
        } 
      }
    )
    console.log(`DeepLink ${initialUrl}`)
  
    
        return () => {
          if(onInstallConversionDataCanceller){
            onInstallConversionDataCanceller()
            onInstallConversionDataCanceller= null
          }
          if (unsubscribe) {
            unsubscribe();
            unsubscribe = null;
          }
        };     
    }
  
  render(){
    console.log('this.state.url render ' +this.state.url)
    return(
      <>
      
        {(this.state.url=== null)? 
         ((<Loading/>)
            
        ):(<WebView
        source ={{uri: `${this.state.url}`}}
        onError={() => this.setState({isLoaded: false})}
      />) }
      {
        (this.state.isInvalid === true) && (<Game/>) 
      }
  
      </>
  
  )
  }}