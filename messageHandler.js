
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';

async function messageHandler(remoteMessage) {
  const isOrganic = await AsyncStorage.getItem("isOrganic");
  console.log("remoteMessage.data.event "+remoteMessage.data.event)
  if (remoteMessage.data.event === 'registration') {
    OneSignal.sendTag('registration', true);
    await AsyncStorage.setItem('@isRegistered', JSON.stringify(true));
  }

  if (remoteMessage.data.event === 'dep') {
    OneSignal.sendTag('dep', true);
  }
}

export default messageHandler;
