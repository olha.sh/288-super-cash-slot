import React from "react";

import { StyleSheet} from "react-native";
import LottieView from "lottie-react-native";

function Loading() {
  return (
    <LottieView
      style={styles.container}
      source={require("./loader.json")}
      autoPlay
      loop
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7BD38",
  },
});

export {
  Loading
} 